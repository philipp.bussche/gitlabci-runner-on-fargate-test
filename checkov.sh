#!/bin/bash
checkov --framework {terraform,terraform_plan} --skip-download -d .
if [ $? -eq 0 ]; then
    echo "Checkov verification successful."
else
    echo "Checkov verification failed."
    # exit 1
fi
