terraform {
  backend "s3" {
    bucket = "tfstate30032022"
    region = "eu-west-3"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

// our primary region to deploy infrastracture to
provider "aws" {
  region = "eu-west-3"
  alias  = "primary"
  # The below will be specific to the application team / account
  # assume_role {
  #  role_arn = "arn:aws:iam::<ACCOUNT_ID>:role/assume_role"
  # }
}

data "aws_region" "primary_region" {
  provider = aws.primary
}

// our secondary region to deploy infrastracture to
provider "aws" {
  region = "eu-west-1"
  alias  = "secondary"
  # The below will be specific to the application team / account
  # assume_role {
  #  role_arn = "arn:aws:iam::<ACCOUNT_ID>:role/assume_role"
  # }
}

data "aws_region" "secondary_region" {
  provider = aws.secondary
}